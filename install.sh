#!/usr/bin/env bash

here=`dirname $0`
cd $here
here=`pwd`

# create symlinks

# mac OS X
rm -f ~/.inputrc
ln -sv $here/.inputrc ~

# bash
rm -f ~/.bashrc ~/.bash_profile ~/.bash_profile.d
ln -sv $here/.bashrc ~
ln -sv $here/.bash_profile ~
ln -sv $here/.bash_profile.d ~

# Git
rm -f ~/.gitconfig
ln -sv $here/.gitconfig ~

# mercurial
rm -f ~/.hgrc ~/.hgignore ~/.hgignore_global
ln -sv $here/.hgrc ~
ln -sv $here/.hgignore ~
ln -sv $here/.hgignore_global ~

# ssh
rm -rf ~/.ssh/config
ln -sv $here/.ssh/config ~/.ssh/

# vim
rm -f ~/.vimrc
ln -sv $here/.vimrc ~
mkdir -p ~/.vim/backups
mkdir -p ~/.vim/swaps
mkdir -p ~/.vim/undo

# wget
rm -f ~/.wgetrc
ln -sv $here/.wgetrc ~
